# thrarch
This is an app for live rendering of 3d nn architectures.

# Run
The app uses Webpack to compile/run. It can be run in development mode using
```bash
npm install
npm run start:dev
```
