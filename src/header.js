let label;

function capitalize(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

// Setup header
export function setupHeader() {
  const parent = document.createElement("div");
  parent.setAttribute("class", "label");

  const info = document.createElement("h2");
  parent.appendChild(info);
  info.innerHTML = "Architecture | Johan Book";

  label = document.createElement("h4");
  parent.appendChild(label);

  document.body.appendChild(parent);
}

export function updateHeader(data) {
  let dims = " ";
  if ("channels" in data) dims += data.channels + "@";
  dims += data.outputs;
  label.innerHTML = capitalize(data.type) + dims;
}
