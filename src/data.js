/* This is an interface to the backend */

const prefix = "http://localhost:5000/classification";

export function getData() {
  const data = {
    0: { type: "input", channels: 1, outputs: [32, 32] },
    1: { type: "conv", channels: 6, outputs: [28, 28] },
    2: { type: "maxpool", channels: 6, outputs: [14, 14] },
    3: { type: "conv", channels: 16, outputs: [10, 10] },
    4: { type: "maxpool", channels: 16, outputs: [5, 5] },
    5: { type: "linear", outputs: 120 },
    6: { type: "linear", outputs: 84 },
    7: { type: "linear", outputs: 10 }
  };
  return data;
}
