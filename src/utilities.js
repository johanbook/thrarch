var THREE = require("three");

function componentToHex(c) {
  var hex = c.toString(16);
  return hex.length == 1 ? "0" + hex : hex;
}

export function rgbToHex(r, g, b) {
  return "0x" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}

export function colorFromMean(mean) {
  if (mean > 0.5) {
    const intensity = Math.round(2 * 255 * (mean - 0.5));
    return rgbToHex(intensity, 0, 0);
  } else {
    const intensity = Math.round(2 * 255 * (0.5 - mean));
    return rgbToHex(0, intensity, 0);
  }
}

function makeLabelCanvas(size, name) {
  const borderSize = 2;
  const context = document.createElement("canvas").getContext("2d");
  const font = `${size}px bold sans-serif`;
  context.font = font;

  // measure how long the name will be
  const doubleBorderSize = borderSize * 2;
  const width = context.measureText(name).width + doubleBorderSize;
  const height = size + doubleBorderSize;
  context.canvas.width = width;
  context.canvas.height = height;

  // need to set font again after resizing canvas
  context.font = font;
  context.textBaseline = "top";

  context.fillStyle = "white";
  context.fillText(name, borderSize, borderSize);

  return context.canvas;
}

export function createText(text, x, y, z, size) {
  const canvas = makeLabelCanvas(size, text);
  const texture = new THREE.CanvasTexture(canvas);
  // because our canvas is likely not a power of 2
  // in both dimensions set the filtering appropriately.
  texture.minFilter = THREE.LinearFilter;
  texture.wrapS = THREE.ClampToEdgeWrapping;
  texture.wrapT = THREE.ClampToEdgeWrapping;

  const labelGeometry = new THREE.PlaneBufferGeometry(1, 1);
  const labelMaterial = new THREE.MeshBasicMaterial({
    map: texture,
    side: THREE.DoubleSide,
    transparent: true
  });

  const root = new THREE.Object3D();
  root.position.x = x;
  root.position.y = y;
  root.position.z = z;

  const label = new THREE.Mesh(labelGeometry, labelMaterial);
  root.add(label);

  // if units are meters then 0.01 here makes size
  // of the label into centimeters.
  const labelBaseScale = 0.01;
  label.scale.x = canvas.width * labelBaseScale;
  label.scale.y = canvas.height * labelBaseScale;

  return root;
}
