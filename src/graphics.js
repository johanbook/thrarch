/* This code is responsible for 3d rendering*/
var THREE = require("three");
import { OrbitControls } from "./OrbitControls.js";
import { setupHeader, updateHeader } from "./header.js";
import { getData, getDummyDataPoints, createNewSession } from "./data.js";
import { colorFromMean, createText, rgbToHex } from "./utilities.js";

const BOX_SIZE = 0.1;

let scene, camera, renderer, controls;
let raycaster, mouse, selected;
let datapoints = [];
let objects = [];

/* Initial setup */
function init(gridSize) {
  window.GRID_SIZE = gridSize;
  window.GRID_POINTS = gridSize * gridSize;

  scene = new THREE.Scene();

  camera = new THREE.PerspectiveCamera(
    75,
    window.innerWidth / window.innerHeight,
    0.1,
    1000
  );
  camera.position.set(0, 5, 0);
  camera.lookAt(0, -10, 0);

  // Setup renderer
  renderer = new THREE.WebGLRenderer({ antialias: true });
  renderer.setSize(window.innerWidth, window.innerHeight);
  document.body.appendChild(renderer.domElement);

  setupHeader();
  controls = new OrbitControls(camera, renderer.domElement);

  // Setup lightning
  const light = new THREE.PointLight(0xffffff, 1, 500);
  light.position.set(10, 0, 5);
  scene.add(light);

  // Raycaster
  raycaster = new THREE.Raycaster();
  mouse = new THREE.Vector2();
}

function getBlock(data) {
  const settings = { name: data.type };
  if (data.type === "linear") {
    settings.xdim = 1;
    settings.ydim = 1;
    settings.zdim = data.outputs;
    settings.color = 0x606060; // gray
  } else if (data.type == "maxpool") {
    settings.xdim = data.channels;
    settings.ydim = data.outputs[0];
    settings.zdim = data.outputs[1];
    settings.color = 0xff0000;
  } else {
    settings.xdim = data.channels;
    settings.ydim = data.outputs[0];
    settings.zdim = data.outputs[1];
    settings.color = 0x000ff;
  }

  if (data.type == "input") settings.color = 0x303030;

  return settings;
}

/* Create a scene */
function setupScene() {
  const data = getData();
  let x = 0;
  for (let idx in data) {
    const conf = getBlock(data[idx]);
    console.log(idx, conf, x);

    var geometry = new THREE.BoxGeometry(conf.xdim, conf.ydim, conf.zdim);
    const material = new THREE.MeshBasicMaterial({
      color: conf.color,
      transparent: true,
      opacity: 0.5
    });

    const cube = new THREE.Mesh(geometry, material);
    cube.position.x = x + conf.xdim / 2;
    cube.data = data[idx];
    cube.color = conf.color;
    scene.add(cube);
    objects.push(cube);

    var geo = new THREE.EdgesGeometry(cube.geometry);
    var mat = new THREE.LineBasicMaterial({
      color: 0x404040,
      linewidth: 2,
      transparent: true,
      opacity: 0.5
    });
    var wireframe = new THREE.LineSegments(geo, mat);
    wireframe.renderOrder = 1; // make sure wireframes are rendered 2nd
    cube.add(wireframe);

    x += conf.xdim + 1;
  }

  // Adjust camera
  camera.lookAt(x / 2, 0, 0);
  camera.position.x = x / 2;
  camera.position.y = 25;
}

/* Animation loop */
async function animate() {
  requestAnimationFrame(animate);
  controls.update();
  renderer.render(scene, camera);
}

/* Event listener for window resizing*/
function onWindowResize() {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
  renderer.setSize(window.innerWidth, window.innerHeight);
}
window.addEventListener("resize", onWindowResize, false);

/* Mouse move events */
function onMouseMove(event) {
  event.preventDefault();
  mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
  mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
  raycaster.setFromCamera(mouse, camera);

  let intersects = raycaster.intersectObjects(objects, false);
  if (intersects.length > 0) {
    // Reset color of previous
    if (selected && selected !== intersects[0].object)
      selected.material.color.setHex(selected.color);

    selected = intersects[0].object;
    selected.material.color.setHex(rgbToHex(150, 150, 150));
    updateHeader(selected.data);
  }
}
window.addEventListener("mousemove", onMouseMove, false);

export async function start() {
  init(16);
  setupScene();
  animate();
}
